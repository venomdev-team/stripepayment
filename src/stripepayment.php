<?php
/**
 * A Stripe gateway payment library for Joomla! v3.
 * 
 * @package Library.venomDev.StripePayment
 * @copyright 2021 venomDev
 * @license LGPL v2
 */

/**
 * Check for a valid Joomla environment
 */
defined('_JEXEC') or die;

$venomdevStripePaymentXML = simplexml_load_file(__DIR__. '/stripepayment.xml');

/**
 * Initialize the library namespace details
 */
define('LIB_VENOMDEV_STRIPEPAYMENT', $venomdevStripePaymentXML->version);
